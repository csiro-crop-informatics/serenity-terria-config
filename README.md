# serenity-terria-config

This repository contains the terriajs config for use in serenity.

The terriajs initialisation catalog is terria.json, which refers to other catalog files found in this directory. These files are live and pulled directly into the Serenity-Terria when viewed. The develop branch catalog can be viewed in the test instance at: https://terriamap-dev.k8s.serenity-csiro.com/ (the catalog may be cached and may take a period of time to update) and the main branch is used in the production instance at: https://terriamap.k8s.serenity-csiro.com/

# Development

Other branches can be viewed via: http(s)://\<server_url>/#clean&<path_to_catalog.json> but requires web security to be disabled

<code>
 chrome --user-data-dir="/tmp/chrome_dev" --disable-web-security
</code>

#### WARNING: Don't log in to any other websites in a window with web security disabled because you don't have cross origin protection active.

Note: The terria catalog refers to sub-catalogs via the full url (i.e "url": "https://gitlab.com/csiro-crop-informatics/serenity-terria-config/-/raw/develop/soil.json") and when merging the develop branch into the main branch paths may need to be updated (.../raw/develop/soil.json -> .../raw/main/soil.json).

# Available Data

# National Soil Maps (soil.json)

The national soil maps have been taken from NationalMap (https://terria-catalogs-public.storage.googleapis.com/nationalmap/catalog/land-cover-land-use/prod.json) 
and are served from the Australian Soil Resource Information System (https://www.asris.csiro.au/).

# On-Farm Sensors (sensors.json)

Sensors at Boorowa are configured to stream data to Senaps (senaps.io). This data is available via the senaps-api and terria via the SenapsLocationsCatalogItem (https://docs.terria.io/guide/connecting-to-data/catalog-type-details/senaps-locations/). Data is fetched via the location id *boorowa*  and Senaps API key is stored as a kubernetes secret (serverconfig.json)

There are currently four configured sensors:
    - Boorowa TempRH lat/lon (id=*boorowa_th*, 96 sensors)
    - Boorowa TempRH Site (id=*boorowa.temprh*, 6 sensors)
    - Boorowa Water Sensor (id=*boorowa*ontoto*, 1 sensor)
    - Boorowa Soil Moisture Sensors (id=*hussat_terrasonde*, 96 sensors, note that these sensors do not contain the location boorowa in the id which may cause issues in the future)
    - Boorowa Weather Station (id=*boorowa*aws*, 1 sensor)

# On-Farm Experiments

Field trial locations, by active year, are sourced from the Serenity experiments registry (API: https://api.serenity-csiro.com or web: https://serenity-csiro.com).

# Adding data

https://docs.terria.io/guide/connecting-to-data/




